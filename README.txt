Terminology
===========

Project: its a module, or a theme. Drupal core included, its the drupal project.
Site: Client site where sentry client is installed
Available projects: all modules or themes in our database, all known modules of the client sites.



Periodic tasks
==============
Plugins can rely on two methods to have their updates handled. The server module implements hook_cron in which it calls hook_sentry_check_all passing all the sites that are due an update.

The update period is configurable for each site on their node edit form. This setting defines how often they are placed in the hook_sentry_check_all() calls.

Plugins can act on this hook call, or alternatively implement their own hook_cron calls. It is generally a good idea to use the hook_cron for tasks that are global, not site specific only.



Sentry API
==========

Writing Plugins
---------------

Announcing information about plugins - hook_plugin_info()

Sentry server provides a hopefully easily extendible interface to write new plugins. A new plugin has to implement the hook_plugin_info hook, a handler function and a theme function for the status of the plugin. The handler function will be called on regular intervals, receiving the node that is about to be updated as argument. XML-RPC callbacks can also be defined here, in that case the server calls the method for the plugin, providing centralised error handling. Should the developer want to use own error handling, just omit declaring the xmlrpc_callback.

The sentry_server module uses this information to automatically call the xml-rpc methods for the plugins, handling all errors in one place. The xmlrpc_callback element is optional however, if not defined only the handler will be called and the entire plugin behaviour is up to the implementation.

A theme function has to be present so the overview page (current name: my sites) can render the status. This should be in the form 'sentry_server_' and the machine friendly status name. This theme function will receive two arguments: the status integer and the node id this status is about.


More details about hook_plugin_info():

Status info has been removed in favour of this hook. It returns overall information about the plugin, its status title (user-friendly name) and name (machine friendly name). Potentially one module can implement multiple plugins, so use a key when defining the plugin information array, similarly to hook_node_info(). 

Example:

function sentry_server_update_plugin_info() {
  $info = array(
    'update_status' => array(
      'title' => t('Update status'),
      'handler' => 'sentry_server_update_site',
      'xmlrpc_callback' => 'drupalSentry.updateStatus',
      'status_name' => 'update_status',
      'status_title' => t('Update status'),
      'description' => t('Obtain a list of installed modules from the client and calculate their up-to-date status.'),
    ));
  return $info;
}

Handling the request in a plugin - plugin handler in hook_plugin_info()

The plugin handler is called after the server issued the XML-RPC call to the client site. It will be passed two arguments: the $node object the server is processing and the $result from the XML-RPC call (such as the list of installed modules and themes for the update_projects plugin).


Reporting statuses - hook_sentry_report()

Return a report with the same syntax as hook_requirements does it on the status of the plugins the module implements. Note that a module can potentially implement more than one plugin, in this case the hook has to implement a second argument which is the plugin name, and should return a report only for that plugin. Example in core line 502.

Status reporting is done on three places:
1. dashboard-like overview (overview of all sites),
2. node view (overview on one site),
3. plugin overview (detailed information e.g. project statuses per project).

Dashboard and node view uses hook_sentry_report to find out the severity of the statuses. The node overview page uses theme_status_report from system.module for this the hook_sentry_report is used. Per project overview is handled independently by the modules by assigning a tab to the node in hook_menu, and defining their own page callbacks.
 

Extensions
==========

Sentry Server Update
--------------------

Downloads release information from drupal.org, parses it and stores it. Fetches version and other module information from the client sites and stores that information as well. At a later stage it compares this two information and updates the locally stored status information based on the comparision. This method is the most complicated component in the module, it is reworked code from update.module.

Individual projects are cached and keyed by project name and major version. This prevents multiple clients from requesting the same project xml from drupal.org thus reducing the server load. Cache expiration is controlled by a variable that is currently non-configurable.


Sentry Server Cron
------------------

Calls the cron.php on the client site using drupal_http_request. It records the return code, timestamp and duration in ms of the call. Display can be a chart (time vs delay) if charts module is present. Table otherwise.

Sentry Server HTTP
------------------

Runs the remote index.php and measures the time it took. Built into the sentry server module.

Sentry Hash
-----------

Store hashes of the module files of the client site to monitor any file tampering attempts.


Planned extensions
==================

Sentry Server Uptime: record the server availability stats in a timeline
