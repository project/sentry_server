<?php

/**
 * @file
 *   Page callbacks related to display of sentry status.
 */
function sentry_server_mysites() {
  // For the constants.
  include_once './includes/install.inc';
  // For the nifty icons.
  drupal_add_css(drupal_get_path('module', 'system') .'/admin.css', 'module');
  global $user;
  $sql = 'SELECT nid FROM {node} WHERE type = "sentry_site" AND uid = %d';
  $sql_count = 'SELECT count(nid) FROM {node} WHERE type = "sentry_site" AND uid = %d';
  $args[] = $user->uid;
  $result = pager_query($sql, 20, 0, $sql_count, $args);
  $nodes = array();

  $available_plugins = module_invoke_all('plugin_info');
  foreach ($available_plugins as $plugin => $plugin_info) {
    $status_titles[] = $plugin_info['status_title'];
  }

  // Table header, for all plugins.
  $header = array_merge(array(t('Title')), $status_titles);

  while ($nid = db_fetch_object($result)) {
    $nodes[$nid->nid] = node_load($nid->nid);
  }

  $rows = array();
  // Collect all the status information for a table
  foreach ($nodes as $node) {
    $row = array();
    $row[] = l($node->title, 'node/'. $node->nid);

    $reports = sentry_get_all_reports($node);
    foreach ($available_plugins as $plugin => $plugin_info) {
      if (isset($reports[$plugin])) { // We have a report for this plugin, yeah!
        $classes = array(
          REQUIREMENT_INFO => 'info',
          REQUIREMENT_OK => 'ok',
          REQUIREMENT_WARNING => 'warning',
          REQUIREMENT_ERROR => 'error',
        );

        $class = $classes[isset($reports[$plugin]['severity']) ? (int)$reports[$plugin]['severity'] : 0];
        $status = array('data' => $reports[$plugin]['value'], 'class' => $class);
      }
      else { // Not enabled.
        $status = t('Not enabled.');
      }
      $row[] = $status;
    }

    $rows[] = $row;
  }
  if (!count($rows)) {
    $cols = count($header);
    $rows[][] = array('colspan' => $cols, 'data' => t('Your monitored sites will appear here.'));
  }

  $output = theme('table', $header, $rows);
  return $output;
}
