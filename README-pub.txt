Public Sentry Documentation

Drupal Sentry is a lightweight, central site monitoring tool. Client sites after installing a lightweight module report information to a single server which provides an overview dashboard and notification for the subscribers.

The client modules are available on drupal.org (http://drupal.org/project/sentry_client), for Drupal 5 and 6, the server code is not public at the moment, but built 100% on Drupal. 

Current status:
- sites represented nodes on server
- plugins API (and an easy to extend server architecture), plugins available: site file hashes, central update status, run cron, index.php timeout.
- client implementations of server plugins
- dashboard for overseeing all of your sites


Features planned:
- encrypted data transfer from client to server
- using messaging API and notifications API to facilitate flexible notification of status changes.

The main objective of the project is to provide a very lightweight (even the client side calls are in separate include files, and have no dependency) solution for the client sites to be monitored from one central location. Because the server uses the sites as nodes, many contributed modules can be used to extend access to the  status report to various levels of users (for instance a drupalshop monitors a group of sites, but wants to grant monitoring access to one of their clients).
