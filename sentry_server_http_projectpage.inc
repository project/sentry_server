<?php

function sentry_server_http_projectpage($node) {
  $output = sentry_server_http_table($node);
  //$output = sentry_server_http_chart($node);
  return $output;
}


function sentry_server_http_chart($node) {
  $sql = 'SELECT delay, timestamp FROM {sentry_http_history} WHERE nid = %d ORDER BY timestamp ASC';
  $result = db_query($sql, $node->nid);

  $data = array();
  while ($entry = db_fetch_object($result)) {
    $data[$entry->timestamp] = $entry->delay;
  }

  $week_chart = sentry_week_chart($data);
  $day_chart = sentry_24h_chart($data);
  return $day_chart . $week_chart;
//  return sentry_chart($data);
}


function sentry_week_chart($data) {
  $g = new open_flash_chart_api();
  $g->set_title(t('Response time in the last week'), '{font-size: 12px;}' );
  $g->set_width(600);
  $week = 7*24*3600;
  $now = time();
  foreach ($data as $k => $v) {
    if ($k >  $now - $week) {
      $delay[] = $v/1000;
      $timestamps[] = format_date($k, 'custom', 'D H:m');
    }
  }

  //
  // BAR CHART:
  //
  $g->set_data( $delay );
  $g->line( 2, '#000', '#000', 'Page views', 10 );
  $g->line_dot( 3, 5, '#000', 'Downloads', 10);    // <-- 3px thick + dots
  //
  // ------------------------
  //

  //
  // X axis tweeks:
  //
  $g->set_x_labels( $timestamps );
  //
  // set the X axis to show every 2nd label:
  //
  $g->set_x_label_style( 10, '#000', 0, 20 );
  //
  // and tick every second value:
  //
  $g->set_x_axis_steps( 2 );
  //

  $g->set_y_max( 10 );
  $g->y_label_steps( 4 );
  $g->set_x_legend( 'Time', 12, '#000' );
  $g->set_y_legend( 'Response time [s]', 12, '#000' );
  return $g->render();
}

function sentry_24h_chart($data) {
  $g = new open_flash_chart_api();
  $g->set_title(t('Response time in the last 24 hours'), '{font-size: 12px;}' );
  $g->set_width(600);
  $day = 24*3600;
  $now = time();
  foreach ($data as $k => $v) {
    if ($k >  $now - $day) {
      $delay[] = $v/1000;
      $timestamps[] = format_date($k, 'custom', 'H:m');
    }
  }

  //
  // BAR CHART:
  //
  $g->set_data( $delay );
  $g->line( 2, '#000', '#000', 'Page views', 10 );
  $g->line_dot( 3, 5, '#000', 'Downloads', 10);    // <-- 3px thick + dots
  //
  // ------------------------
  //

  //
  // X axis tweeks:
  //
  $g->set_x_labels( $timestamps );
  //
  // set the X axis to show every 2nd label:
  //
  $g->set_x_label_style( 10, '#000', 0, 2 );
  //
  // and tick every second value:
  //
  $g->set_x_axis_steps( 2 );
  //

  $g->set_y_max( 10 );
  $g->y_label_steps( 4 );
  $g->set_x_legend( 'Time', 12, '#000' );
  $g->set_y_legend( 'Response time [s]', 12, '#000' );
  return $g->render();
}

function sentry_chart($data) {
  $width = 500;
  $max = sentry_chart_max($data);

  $scale = $width/$max;

  foreach ($data as $k => $v) {
    $w = (int)($v * $scale);
    $timeout = $v/1000 . ' s'; // $v is in microseconds
    $output .= '<hr style="height: 1px; width: '. $w .'px; text-align: right; background-color: #000"/>';
  }
  return $output;
}

function sentry_chart_max($data) {
  $max = 0;
  foreach ($data as $k => $v) {
    $max = $max < $v ? $v : $max;
  }
  return $max;
}

function sentry_server_http_table($node) {
  $sql = 'SELECT delay, timestamp FROM {sentry_http_history} WHERE nid = %d ORDER BY timestamp DESC';
  $sql_count = 'SELECT count(timestamp) FROM {sentry_http_history} WHERE nid = %d';
  $args[] = $node->nid;
  $result = pager_query($sql, 20, 0, $sql_count, $args);
  $rows = array();
  while ($data = db_fetch_object($result)) {
    $row = array(
      format_date($data->timestamp),
      $data->delay . ' ms',
    );
    $rows[] = $row;
  }
  $header = array(t('Date'), t('Delay'));
  $pager = theme('pager');
  return $pager . theme('table', $header, $rows) . $pager;
}
