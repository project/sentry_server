<?php

function sentry_hash_overview($node) {
  $result = db_query('SELECT filename, hash, temporary_hash FROM {sentry_hash_clients} WHERE nid = %d', $node->nid);

  $is_error = FALSE;
  $rows = array();
  while ($file = db_fetch_object($result)) {
    $row = array();

    if ($file->hash != $file->temporary_hash) {
      $class = 'error';
      $icon = theme('image', 'misc/watchdog-error.png', t('error'), t('error'));
      $text = t('File changed!');
      $is_error = TRUE;
    }
    else {
      $class = 'ok';
      $icon = theme('image', 'misc/watchdog-ok.png', t('ok'), t('ok'));
      $text = t('File safe.');
    }
    $row = array(basename($file->filename), $text . $icon);
    $rows[] = $row;
  }
  if (!count($rows)) {
    $rows[][] = array('data' => t('No hashes have been recorded yet.'), 'colspan' => 2);
  }

  $header = array(t('Filename'), t('Status'));
  if ($is_error) {
    $output = drupal_get_form('sentry_hash_approval_form', $node);
  }
  $output .= theme('table', $header, $rows);
  return $output;
}

function sentry_hash_approval_form($node) {
  $form = array();
  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => $node->nid,
  );
  $form['submit'] = array(
    '#value' => t("It's ok"),
    '#type' => 'submit',
  );
  return $form;
}

function sentry_hash_approval_form_submit($form, &$form_state) {
  db_query('UPDATE {sentry_hash_clients} SET hash = temporary_hash WHERE nid = %d', $form_state['values']['nid']);
}
