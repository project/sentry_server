<?php

function sentry_hash_settings_form() {
  $form = array();
  $period = drupal_map_assoc(array(3600, 7200, 14400, 43200, 86400), 'format_interval');
  $form['sentry_hash_period'] = array(
    '#type' => 'select',
    '#options' => $period,
    '#title' => t('Control period'),
    '#description' => t('Time interval between two hash checks.'),
    '#default_value' => variable_get('sentry_hash_period', 43200),
  );
  return system_settings_form($form);
}
