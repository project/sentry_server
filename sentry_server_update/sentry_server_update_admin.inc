<?php

function sentry_server_update_settings_form() {
  $form = array();
  $period = drupal_map_assoc(array(3600, 7200, 14400, 43200, 86400), 'format_interval');
  $form['sentry_update_period'] = array(
    '#type' => 'select',
    '#options' => $period,
    '#title' => t('Update period'),
    '#description' => t('Update period for project releases, fetching from durpal.org. Be careful, we would not want to abuse it by downloading massive release infos frequently.'),
    '#default_value' => variable_get('sentry_update_period', 43200),
  );
  return system_settings_form($form);
}
