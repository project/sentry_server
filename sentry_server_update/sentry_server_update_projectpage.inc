<?php

function sentry_server_update_projectpage($node) {
  $output .= l(t('Download'), 'node/' . $node->nid . '/projects/download');
  $output .= theme('sentry_project_status', $node);
  return $output;
}

function sentry_server_update_download($node) {
  $header = array(t('Project type'), t('Module'), t('Installed version'), t('Status'));
  $statuses = _sentry_server_update_statuses();
  $rows = array();
  if (isset($node->projects)) {
    foreach ($node->projects as $project) {
      if ($project['title'] == '') {
        continue;
      }
      $row = array(
        $project['project_type'],
        $project['title'],
        $project['version'],
        $statuses[$project['status']]['title'],
      );
      $rows[] = $row;
    }
  }
  print sentry_server_create_csv($header, $rows, $node);
  exit;
}

function sentry_server_create_csv($header, $rows, $node, $separator = ',', $delimiter = '"') {
  $site_name = strtolower(str_replace(' ', '-', $node->title));
  $filename = $site_name . '-update-report-' . date("Y-m-d") . '.csv';

  header("Content-type: text/csv");
  header('Content-Disposition: attachment; filename="'. $filename .'"');

  $output = $delimiter . implode('"'. $separator .'"', $header) . $delimiter ."\n"; // header
  foreach ($rows as $row) { // loop through all rows
    $output .= $delimiter . implode('"'. $separator .'"', $row) . $delimiter . "\n"; // create CSV of rows
  }

  return $output;
}
