<?php

function sentry_server_cron_projectpage($node) {
  if (module_exists('charts')) {
    $output = sentry_server_cron_chart($node);
  }
  else {
    $output = sentry_server_cron_table($node);
  }
  return $output;
}

function sentry_server_cron_table($node) {
  $sql = 'SELECT delay, timestamp FROM {sentry_cron_history} WHERE nid = %d ORDER BY timestamp DESC';
  $sql_count = 'SELECT count(timestamp) FROM {sentry_cron_history} WHERE nid = %d';
  $args[] = $node->nid;
  $result = pager_query($sql, 20, 0, $sql_count, $args);
  $rows = array();
  while ($data = db_fetch_object($result)) {
    $row = array(
      format_date($data->timestamp),
      $data->delay . ' ms',
    );
    $rows[] = $row;
  }
  $header = array(t('Date'), t('Delay'));
  $pager = theme('pager');
  return $pager . theme('table', $header, $rows) . $pager;
}

function sentry_server_cron_chart($node) {
  $result = db_query('SELECT delay, timestamp FROM {sentry_cron_history} WHERE nid = %d ORDER BY timestamp DESC', $node->nid);
  $chart = array(
    '#title' => t('Cron responses'),
    '#plugin'    => 'google', // Google Charts API will be used
    '#type'     => 'line2D', // To show a simple 2D line chart
    '#height'   => 300, // in pixels
    '#width'    => 600, // in pixels
  );
  $chart[0]['#legend'] = t('Timestamp');
  $chart[1]['#legend'] = t('Delay');
  while ($row = db_fetch_object($result)) {
    $delay = $row->delay/1000; // converting to seconds
    $chart[0][] = array(
      '#value' => $row->timestamp,
      '#label' => format_date($row->timestamp, 'small'),
    );
    $chart[1][] = array(
      '#value' => $delay,
      '#label' => format_plural('1 second', '@count seconds', $row->delay),
    );
  }
  return charts_chart($chart);
}
