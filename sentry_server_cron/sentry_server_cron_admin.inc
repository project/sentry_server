<?php

function sentry_server_cron_settings_form() {
  $form = array();
  $period = drupal_map_assoc(array(604800, 1209600, 1814400, 2419200, 3024000), 'format_interval');
  $form['sentry_cron_history'] = array(
    '#type' => 'select',
    '#options' => $period,
    '#title' => t('Cron history'),
    '#description' => t('Time until cron history logs are stored.'),
    '#default_value' => variable_get('sentry_cron_history', 604800),
  );
  return system_settings_form($form);
}

